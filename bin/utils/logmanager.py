#!/usr/bin/python
# -*- coding: utf-8 -*-
import datetime, os, time
import logging
import traceback
import tarfile
import resource
import serial
from utils.tools import *

class UartHandler(logging.StreamHandler):
    def emit(self, record):
        try:
            msg = self.format(record)
            stream = self.stream
            print(msg)
            msg = str.encode(msg+'\r\n')
            stream.write( msg )
            self.flush()
        except RecursionError:  # See issue 36272
            raise
        except Exception:
            self.handleError(record)
  

def save(type, level, msg):
  #print(type)
  logging.getLogger(type).log(level, msg)

def service(**dicargs):
  #thread_name_init()
  prefix_path = '../logs/'
  if not os.path.isdir(prefix_path):
    os.mkdir(prefix_path)
  #if dicargs.get('console_output') != None:
  #  console = UartHandler(serial.Serial(port=dicargs['console_output']))
  #else:
  #  console = logging.StreamHandler()
  #console.setLevel(logging.INFO)
  #console.setFormatter(logging.Formatter('%(asctime)s.%(msecs)03d [%(levelname)-.4s] %(message)s (%(name)s)','%H:%M:%S'))
  #logging.getLogger('root').addHandler(console)
  tm = None
  nm = None
  while True:
    #一天進入一次
    if tm is None or datetime.datetime.now() > tm :
      #設定記錄檔名稱
      #print logging.getLogger('root').handlers
      for sort in dicargs['task']:
        #print(sort)
        path = prefix_path + sort
        if not os.path.isdir(path):
          os.mkdir(path)
        name = 'root.' + sort
        if sort == 'logging':
          name = 'root'
        logger = logging.getLogger(name)
        logger.setLevel(logging.INFO)
        if sort == 'logging':
          logger.setLevel(logging.INFO)
        
        #logger.setLevel(dicargs['task'][sort]['level']) # 自定義
        for handler in logger.handlers:
          handler.close()
          logger.removeHandler(handler)
          
        filename = datetime.datetime.now().strftime('../logs/' + sort + '/' + sort + 'Log_%Y-%m-%d.log')
        symlinkname = '../logs/' + sort + '.log'
        handler = logging.FileHandler(filename, 'a+')
        if os.path.isfile(symlinkname) or os.path.islink(symlinkname):
          os.remove(symlinkname)
        os.symlink( filename, symlinkname )
        #handler.setLevel(logging.DEBUG)
        #if dicargs['task'][sort].get('formatter', None) == None:
        #  dicargs['task'][sort]['formatter'] = logging.Formatter(dicargs['task'][sort]['format'],'%H:%M:%S')
        #handler.setFormatter(dicargs['task'][sort]['formatter'])
        handler.setFormatter(logging.Formatter('%(asctime)s.%(msecs)03d [%(levelname)-.4s] %(message)s','%H:%M:%S'))
        if sort == 'logging':
          handler.setFormatter(logging.Formatter('%(asctime)s.%(msecs)03d [%(levelname)-.4s] %(message)s (%(name)s)','%H:%M:%S'))
        logger.addHandler(handler)
        '''
        if len(logger.handlers) >= 1:
          print '-ttttt-'
          logger.handlers[0].flush()
          logger.handlers[0].emit()
        else:
          handler = logging.FileHandler(datetime.datetime.now().strftime('../logs/' + sort + '/' + sort + 'Log_%Y-%m-%d.log'), 'a+')
          logger.setLevel(dicargs['task'][sort]['level']) # 自定義
          handler.setLevel(logging.DEBUG)
          if dicargs['task'][sort].get('formatter', None) == None:
            dicargs['task'][sort]['formatter'] = logging.Formatter(dicargs['task'][sort]['format'],'%H:%M:%S')
          handler.setFormatter(dicargs['task'][sort]['formatter'])
          logger.addHandler(handler)
        '''
        #print logger.handlers
            
      tm = get_today_midnight()
      logging.getLogger('root.logging').log( logging.INFO, '初始化完成.' )
      #壓縮該壓縮的檔案, 不過這樣 00:00 系統可能會很忙 (?)
      try:
        for sort in dicargs['task']:
          path = prefix_path + sort 
          using = datetime.datetime.now().strftime( sort + 'Log_%Y-%m-%d.log')
          files = os.listdir( path )
          files.sort()
          for file in files:
            if file[-6:] != 'tar.gz' and file != using:
              absolute_path = path + '/' + file
              tf = tarfile.open( absolute_path + ".tar.gz", "w:gz")
              tf.add( absolute_path, arcname=file )
              tf.close()
              os.remove( absolute_path )
              logging.getLogger('root.logging').log( logging.INFO, '壓縮: %s => %s' % ( file, file + ".tar.gz" ) )
              
      except:
        logging.getLogger('root.logging').log( logging.ERROR, '封存記錄檔發生異常.\n%s' % traceback.format_exc() )
        
      #檢查目錄是否超過 20 MB, 超過就刪排序後最前面的檔案.
      try:
        for sort in dicargs['task']:
          total_byte = 0
          path = prefix_path + sort 
          using = datetime.datetime.now().strftime( sort + 'Log_%Y-%m-%d.log')
          files = os.listdir( path )
          #files.sort(reverse = True)
          dictfiles = {}
          #直接算 using 會有誤差.
          #total_byte = total_byte + os.path.getsize( path + '/' + using )
          
          target_file = None
          for file in files:
            absolute_path = path + '/' + file
            dictfiles[file] = os.path.getctime(absolute_path)
            
          x = sorted(list(dictfiles.items()), key=lambda d:d[1], reverse=True)
          
          for y in x:
            absolute_path = path + '/' + y[0]
            if y[0] != using:
              total_byte = total_byte + os.path.getsize( absolute_path )
              logging.getLogger('root.logging').log( logging.DEBUG, 'add %s, total: %d.' % (y[0], total_byte))
              if total_byte >= dicargs.get( 'max_directory_bytes', 20 * 1024 * 1024 ):
                target_file = y[0]
                break
          
          if target_file != None:
            for y in reversed(x):
              absolute_path = path + '/' + y[0]
              if y[0] != using:
                logging.getLogger('root.logging').log( logging.INFO, '刪除: %s ' % ( y[0] ) )
                os.remove( absolute_path )
              if target_file == y[0]:
                break
      except:
        logging.getLogger('root.logging').log( logging.ERROR, '刪除封存檔發生異常.\n%s' % traceback.format_exc() )
        
    #一分鐘進入一次, 
    #檢查當下 LOG 是否超過 10 MB, 有就改名成 $org_partN.
    
    if nm is None or time.time() > nm :
      nm = time.time() + 60 
      try:
        for sort in dicargs['task']:
          path = prefix_path + sort
          filename = datetime.datetime.now().strftime( sort + 'Log_%Y-%m-%d.log')
          absolute_path = path + '/' + filename
          dictfiles = {}
          if os.path.isfile( absolute_path ) and os.path.getsize( absolute_path ) > dicargs.get( 'max_file_bytes', 10 * 1024 * 1024 ):
            files = os.listdir( path )
            for file in files:
              check_absolute_path = path + '/' + file
              dictfiles[file] = os.path.getctime(check_absolute_path)
              
            x = sorted(list(dictfiles.items()), key=lambda d:d[1], reverse=True)
            num = 1
            for y in x:
              if y[0].find(filename) >= 0 and y[0].find('part') >= 0:
                index = y[0].find('part') + 4
                num = int( y[0][index:].split('.')[0] ) + 1
                break
            tmp_absolute_path = '%s_part%d' % (absolute_path, num)
            
            logging.getLogger('root.logging').log( logging.INFO, '更名: %s => %s' % (absolute_path.split('/')[-1], tmp_absolute_path.split('/')[-1]) )
            os.rename(absolute_path, tmp_absolute_path )
            tm = None #重置 FileHandler
      except:
        logging.getLogger('root.logging').log( logging.ERROR, '檢查紀錄檔大小發生異常.\n%s' % traceback.format_exc() )
       
    time.sleep(1)
    
if __name__ == '__main__':
  pass