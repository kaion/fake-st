#!/usr/bin/python
# -*- coding: utf-8 -*-
import threading
import socketserver
import http.server
import json
import sys
import os
import urllib.parse
from . import tools
rsp_func = None

class MyHandler(http.server.BaseHTTPRequestHandler):
  def do_HEAD(s):
    s.send_response(200)
    s.send_header("Content-type", "text/html")
    s.end_headers()
      
  def do_GET(s):
    parsed_url = urllib.parse.urlparse(s.path)
    query = urllib.parse.parse_qs(parsed_url.query)
    s.send_response(200)
    s.end_headers()
    if rsp_func is not None:
        s.wfile.write(rsp_func(parsed_url.path[1:]))
      
  def do_POST(s):
    parsed_url = urllib.parse.urlparse(s.path)
    query = urllib.parse.parse_qs(parsed_url.query)
    content_len = int(s.headers.getheader('content-length', 0))
    s.send_response(200)
    s.end_headers()
    post_body = s.rfile.read(content_len)
    if rsp_func is not None:
      s.wfile.write(rsp_func(post_body))
      
  def log_message(self, format, *args):
    tools.thread_name_init('http-client')
    pass
        
        
class ThreadingSimpleServer(socketserver.ThreadingMixIn, http.server.HTTPServer):
  pass

def test(response_function):
  global rsp_func
  rsp_func = response_function
  server = ThreadingSimpleServer(('', 4758), MyHandler)
  server_thread = threading.Thread(target=server.serve_forever)
  server_thread.daemon = True
  server_thread.start()
  
  try:
    while True:
      sys.stdout.flush()
      server.handle_request()
  except KeyboardInterrupt:
    logger.info('^C received, shutting down server')
    server.socket.close()