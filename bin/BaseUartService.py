#!/usr/bin/python
# -*- coding: utf-8 -*-
import fcntl, serial
import sys, os
import time
import uuid
import traceback
import logging
import threading

class UartService:

  def __init__(self, dicargs):
    self.cardinal_t = 5
    self.uart = {}
    self.history = dict()
    self.queue = list()
    self.rx_buffer = b''
    self.console = None
    self.thread = None
    self.console_state = None
    self.console_open_prev = 0
    self.current_tid = None
    self.init_state = False
    self.logging = dicargs.get('logging', self._print)
    self.logname = "root.logging"
    self.console_parameter(dicargs)

    
  def _print(self, type, level, msg):
    print(msg)
    
#  def _logging(self, type, level, msg):
#    pass
    
  def _null(self, type, level, msg):
    pass

  def chksum_calc(self, data):
    i = 0
    chksum = ord('\x00')
    while i < len(data):
      chksum = chksum ^ ord(data[i:i+1])
      i += 1
    return chksum
    
  def request_protocol_format(self, data):
    return self.sign_res( None, True, data=data )
  
  def response_paser(self, data):
    return self.sign_res( None, True, data=data )
  
        
  def console_set_state(self, message):
    if self.console_get_state() != message:
      self.logging(self.logname, logging.INFO, 'CONSOLE 狀態由 %s 轉變為 %s .' % ( self.console_state, message ) )
      self.console_state = message
  
  def console_get_state(self):
    return self.console_state
    
  def console_close(self, message=None):
    self.console.close()
    self.console = None
    if message is not None:
      self.console_set_state(message)
    else:
      self.console_set_state('CLOSE')
  
  def console_open(self):
    # 最多一秒開一次.
    if time.time() > self.console_open_prev + 1:
      self.console_open_prev = time.time()
      try:
        self.console = serial.Serial(port=self.uart['port'],baudrate=self.uart['baudrate'], bytesize=self.uart['bytesize'], parity=self.uart['parity'], stopbits=self.uart['stopbits'], timeout=self.uart['timeout'], xonxoff=0, rtscts=0)
        
      except Exception as e:
        self.logging(self.logname, logging.ERROR, '開啟 %s 發生異常.' % self.uart['port'] )
        self.console_set_state('FAILED')
        return False
      #self.console.set_low_latency_mode( True )
      
      #檢查 BUSY
      if self.console is not None:
        if self.console.isOpen():
          import fcntl
          try:
            fcntl.flock(self.console.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
          except IOError:
            print(traceback.format_exc())
            self.console_close('BUSY')
            self.logging(self.logname, logging.ERROR, '設備 %s 忙碌中.' % self.uart['port'] )
            return False
            
      self.console_set_state('ACTIVE')
      return True
      
    else:
      #time.sleep(0.02)
      return False
  def get_tid(self):
    return str(uuid.uuid4())
    
  def tx_process( self, this_uuid ):
    result = None
    console_type = None
    if this_uuid in self.queue:
      self.queue.remove(this_uuid)
      
    if self.console is not None:
      console_type = {'port': self.console.port,'config': str(self.console.baudrate) + '-' + str(self.console.bytesize) + '-'+ self.console.parity + '-' + str(self.console.stopbits), 'timeout': self.console.timeout}
      self.current_tid = this_uuid
      if type(self.history[this_uuid]['send']["data"]) == list:
        for data in self.history[this_uuid]['send']["data"]:
          self.console_write( data, this_uuid )
          time.sleep(0.1) #不停一下 BLE 會受不了
      else:
        self.console_write( self.history[this_uuid]['send']["data"], this_uuid )
      self.logging(self.logname, logging.DEBUG, 'CONSOLE 狀態正常, 結束 %s 交易.' % ( this_uuid ))
      result = True
      
    else:
      self.logging(self.logname, logging.DEBUG, 'CONSOLE 狀態異常, 放棄 %s 交易.' % ( this_uuid ))
      result = False
      
    self.history[this_uuid]['pipe'] = self.sign_res( None, result, console=console_type, state=self.console_state )
    if result == False:
      self.sign_res( self.history[this_uuid], False )
    return result
      
  def tx_process_queue(self, this_uuid, insert=False ):
    self.logging(self.logname, logging.DEBUG, 'REQUEST 產生 %s 交易機會.' % this_uuid )
    if insert is True:
      self.queue.insert( 0, this_uuid )
      
    else:
      self.queue.append( this_uuid )

  def console_write( self, data, this_uuid="" ):
    self.console.write(data)
    self.logging(self.logname, logging.INFO, '送出 %s .' % self.hex_conv_string(data))
  
  def console_read(self):
    if self.console is None:
      if self.console_open() is False:
        return b''
    try:
      rb = self.console.read(1)
    except Exception as e:
      self.logging(self.logname, logging.ERROR, '異常錯誤: %s' % e )
      self.console_close()
      return b''
    return rb
  
  def console_parameter(self, dicargs):
    default_conf = {'port':'/dev/ttyUSB0','baudrate':'115200','bytesize':8,'parity':'N','stopbits':1,'timeout':0.02}
    for parameter in default_conf:
      if dicargs.get(parameter, False) == False:
        self.uart[parameter] = default_conf[parameter]
      else:
        self.uart[parameter] = dicargs[parameter]
      #self.logging(self.logname, logging.DEBUG, '參數: %s, 值: %s' % (parameter, self.uart[parameter]) )
      
  def sign_res(self, d, res, **dicargs):
    if d == None:
      d = {}
    d['_res'] = res
    d['ts'] = '%0.3f' % time.time()
    for a in dicargs:
      d[a] = dicargs[a]
    return d
    
  def hex_conv_string(self, data):
    return_string = ''
    if( type(data) != bytes ):
      return '!!! Not BYTES DATA !!!'
    for x in data:
      return_string += ' %02x' % x
    return return_string[1:]
  def get_tree(self, ts=None):
    if ts == None:
      ts = self.cardinal_t
    return {'_res': None, 'send':None, 'pipe': None, 'recv': None, 'wait': '%0.3f' % float(ts), 'timeout': '%0.3f' % ( time.time() + float(ts)) }
    
  def request_process(self, cmd_prefix, **dicargs):
    this_requ = self.get_tree()
    this_requ['send'] = self.request_protocol_format(cmd_prefix)
    if this_requ['send']['_res'] == False: 
      self.sign_res( this_requ, False )
      return this_requ
    
    this_data = this_requ['send']["data"]
    if dicargs.get('cache', False) is True:
      for a in self.history.keys():
        try:
          if self.history[a]['_res'] is True and self.history[a]['send']["data"] == this_data:
            self.logging(self.logname, logging.DEBUG, 'REQUEST 發現指令 %s 有 CACHE 存檔, 直接回應.' % self.hex_conv_string( this_data ) )
            return self.history[a]
        except Exception as e:
          self.logging(self.logname, logging.CRITICAL , traceback.format_exc() )
    
    this_tid = self.get_tid()
    self.history[this_tid] = this_requ
    
    if dicargs.get( 'no_wait' ):
      self.tx_process_queue( this_tid, False )
    else:
      self.tx_process( this_tid )
    
    #這裡要改寫, 或許 select 比較適合.
    while self.history[this_tid]['_res'] is None:
      if dicargs.get( 'no_wait', False ):
        self.logging(self.logname, logging.DEBUG, 'REQUEST 被設定為不等待 %s 交易結果.' % this_tid )
        break
      time.sleep(0.001)
      
    self.logging(self.logname, logging.DEBUG, 'REQUEST 回報 %s 交易結果.' % this_tid )
    #print(self.history[this_tid])
    return self.history[this_tid]
    
  def rx_process(self):
    self.logging(self.logname, logging.INFO, '收到 ' + self.hex_conv_string(self.rx_buffer) )
    #self.console_write(self.rx_buffer)
    self.rx_buffer = b''

  def loop(self):
    self.logging(self.logname, logging.INFO, '服務啟動.' )
    #self.current_tid = None
    try:
      while True:
        self.rx_byte = self.console_read()
        if self.rx_byte != b'':
          self.rx_buffer += self.rx_byte
          self.rx_process()
          continue
        ts = time.time()
            
        if self.current_tid != None and self.history[self.current_tid]['_res'] != None:
          self.current_tid = None
        
        if self.current_tid == None:
          # 整理記憶體
          
          for a in list(self.history.keys()):
            #tid_ts = self.history[a].get('timeout', 0)
            # History 保留到 timeout 時間 + 5 秒.
            if ts >= float( self.history[a]['timeout'] ) + 5:
              self.logging(self.logname, logging.DEBUG, '回收 %s 記憶體.' % (a))
              del self.history[a]
          # 定期動作
  
          if self.console_get_state() == 'ACTIVE':
            if self.init_state == False:
              self.target_init()
              self.init_state = True
            else:
              self.target_idle(ts)
          
          # 檢查 tx 隊列
          if len(self.queue) > 0:
            for this_uuid in self.queue:
              if self.history.get( this_uuid ) != None:
                self.logging(self.logname, logging.DEBUG, '序列 %s 執行.' % (this_uuid))
                self.tx_process( this_uuid )
                break
              else:
                self.logging(self.logname, logging.DEBUG, '序列 %s 已消失, 自動移除.' % (this_uuid))
                self.queue.remove( this_uuid )
          
            self.logging(self.logname, logging.DEBUG, '剩餘序列 %s .' % (self.queue))
            
        else: #elif self.current_tid != None:
          # 逾時處理. 
          try:
            if ts >= float( self.history[self.current_tid]['timeout'] ):
              self.sign_res( self.history[self.current_tid]['recv'], False, msg='RESPONSE_TIMEOUT' ) 
              self.sign_res( self.history[self.current_tid], False ) 
              self.logging(self.logname, logging.DEBUG, '對象超過 %0.2f 秒無回應, 放棄 %s 交易. (%s)' % (self.cardinal_t, self.current_tid, self.history[self.current_tid]['send']['data'] ))
              self.target_timeout(self.history[self.current_tid])
              self.current_tid = None
          except:
            self.logging(self.logname, logging.WARNING , '逾時處理異常:\n %s' % traceback.format_exc() )
    except:
      self.logging(self.logname, logging.CRITICAL , '主迴圈異常:\n %s' % traceback.format_exc() )
      exit(0)
        
  def target_idle(self,ts):
    pass
    
  def target_init(self):
    pass
  
  def target_timeout(self, data):
    pass
    
  def start(self,name=""):
    if self.thread is None or self.thread.isAlive() is False:
      self.current_tid = None
      self.rx_buffer = b''
      self.thread = threading.Thread(target=self.loop)
      self.thread.daemon = True
      self.thread.setName( '%s' % (name) )
      self.logname = "root." + name
      self.thread.start()
    # 等 CONSOLE READY
    while self.console_get_state() == None:
      #print('.')
      time.sleep(0.001)
    return self.thread

  
if __name__ == '__main__':
  import time
  from pprint import pprint
  reader = UartService({'port': '/dev/smd9'})
  reader.start()
  while True:
    reader.console_write(b'at\r')
    time.sleep(10)