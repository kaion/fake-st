#!/usr/bin/python
# -*- coding: utf-8 -*-
from BaseUartService import UartService
import logging
import codecs
import threading

class Service(UartService):
  status = {}
  cmd_result = []
  prev_ts = 0
  def request_process(self, cmd, **dicargs):
    self.logging(self.logname, logging.DEBUG, '準備送出 ' + cmd.decode() )
    return UartService.request_process(self, cmd + b'\r', **dicargs)
    
  def rx_process(self):
    if self.rx_byte == b'\n':
      self.logging('root.target_uart', logging.INFO, '收到 ' + self.rx_buffer[:-2].decode() )
      if self.rx_buffer[0:1] == b'+' and self.rx_buffer.find( b'CME ERROR' ) < 0:
        name, value = self.rx_buffer[1:].decode().strip('\r\n').split(': ')
        self.status[name] = value
        
      elif self.rx_buffer != b'\r\n' or self.rx_buffer.find( b'OK' ) >= 0 or self.rx_buffer.find( b'ERROR' ) >= 0 :
        self.cmd_result.append(self.rx_buffer.decode().strip('\r\n'))
        #print(self.rx_buffer.find( b'ERROR' ))
        if self.rx_buffer.find( b'OK' ) >= 0 or self.rx_buffer.find( b'ERROR' ) >= 0:
          self.history[self.current_tid]['recv'] = self.response_paser(self.cmd_result)
          self.history[self.current_tid]['retcode'] = True
          if self.rx_buffer.find( b'ERROR' ) >= 0:
            self.history[self.current_tid]['retcode'] = False
          self.sign_res( self.history[self.current_tid], self.history[self.current_tid]['recv']['_res'] )
          self.status_update(self.history[self.current_tid])
          self.cmd_result = []
        
      self.rx_buffer = b''
      return False
    return True
#      
#  def do_QIND(self, value):
#    print(value)
#    
#  def do_CTZE(self, value):
#    pass
#    
  def status_update(self, data):
    this_cmd = data['send']['data'][3:-1].decode().strip('?').split('=')[0]
    #print(data)
    if data['retcode'] == False:
      if self.status.get(this_cmd) != None:
        del self.status[this_cmd]
      if this_cmd == 'CPIN': 
        self.status['CME'] = data['recv']['data'][0].split(': ')[1]
        
    elif this_cmd == 'CGSN': 
      self.status['CGSN'] = data['recv']['data'][0]
      
    elif this_cmd == 'CPIN':
      if self.status.get('CME') != None:
        del self.status['CME']
      
  def target_timeout(self, data):
    status_update(self, data)
    pass
    
  def organize(self):
    self.status['organize'] = {}
      
    qeng = self.status.get('QENG')
    if qeng != None:
      values = qeng.replace('"','').split(',')
      names = []
      if len(values) < 3:
        #print("---------")
        #print(values)
        #print("---------")
        names = ["servingscell", "state"]
      elif values[2] == 'LTE':
        #print("-1-")
        names = ["servingscell", "state", "LTE", "is_tdd", "mcc", "mnc", "cellid", "pcid", "earfcn", "freq_band_ind", "ul_bandwidth", "dl_bandwidth", "tac", "rsrp", "rsrq", "rssi", "sinr", "srxlev"]
      elif values[2] == "GSM":
        names = ["servingscell", "state", "GSM", "mcc", "mnc", "lac", "cellid", "bsic", "arfcn", "band", "rxlev", "txp", "rla", "drx", "c1", "c2", "gprs", "tch", "ts", "ta", "maio", "hsn", "rxlevsub", "rxlevfull", "rxqualsub", "rxqualfull", "voicecodec"]
      elif values[2] == "WCDMA":
        names = ["servingscell", "state", "WCDMA", "mcc", "mnc", "lac", "cellid", "uarfcn", "psc", "rac", "rscp", "ecio", "phych", "sf", "slot", "speech_code", "comMod"]
      elif values[2] == "TDSCDMA":
        names = ["servingscell", "state", "TDSCDMA", "mcc", "mnc", "lac", "cellid", "pfreq", "rssi", "rscp", "ecio"]
      elif values[2] == "CDMA":
        names = ["servingscell", "state", "CDMA", "mcc", "mnc", "lac", "cellid", "bcch", "rxpwr", "ecio", "txpwr"]
      elif values[2] == "HDR":
        name = ["servingscell", "state", "HDR", "mcc", "mnc", "lac", "cellid", "bcch", "rxpwr", "ecio", "txpwr"]
      self.status['organize']['QENG'] = {}
      for x in range(0,len(values)):
        if x != 0 and x != 2:
          self.status['organize']['QENG'].update({names[x]: values[x]})
    qnwinfo = self.status.get('QNWINFO')
    if qnwinfo != None:
      values = qnwinfo.replace('"','').split(',')
      names = ["act", "oper", "band", "channel"]
      self.status['organize']['QNWINFO'] = {}
      for x in range(0,len(values)):
        self.status['organize']['QNWINFO'].update({names[x]: values[x]})
        
    cops = self.status.get('COPS')
    if cops != None:
      values = cops.replace('"','').split(',')
      names = ["mode", "format", "oper", "act"]
      self.status['organize']['COPS'] = {}
      for x in range(0,len(values)):
        self.status['organize']['COPS'].update({names[x]: values[x]})
        
    csq = self.status.get('CSQ')
    if csq != None:
      values = csq.replace('"','').split(',')
      names = ["rssi", "ber"]
      self.status['organize']['CSQ'] = {}
      #print(values)
      for x in range(0,len(values)):
        self.status['organize']['CSQ'].update({names[x]: values[x]})

    qspn = self.status.get('QSPN')
    if qspn != None:
      values = qspn.replace('"','').split(',')
      names = ["FNN", "SNN", "SPN", "alphabet", "RPLMN"]
      self.status['organize']['QSPN'] = {}
      utf_16_be = False
      for x in range(len(values)-1,0-1,-1):
        if utf_16_be == False:
          self.status['organize']['QSPN'].update({names[x]: values[x]})
        else:
          self.status['organize']['QSPN'].update({names[x]: codecs.decode(bytes.fromhex(values[x]),encoding='utf_16_be')})
        if names[x] == "alphabet" and values[x] == '1':
          utf_16_be = True
  
  def target_init(self):
    for cmd in [b'ATE0', b'ATV1', b'AT+CMEE=1', b'AT+CGREG=2', b'AT+QURCCFG="urcport","uart2"']:
      self.request_process(cmd, no_wait=True)
      
      
  def target_idle(self, ts):
    #this_ts = int(time.time())
    #print(ts)
    if ts > self.prev_ts:
      for cmd in [ b'AT+CPIN?', b'AT+CSQ', b'AT+COPS?', b'AT+QSPN', b'AT+QCCID', b'AT+QNWINFO', b'AT+CGPADDR=1', b'AT+QENG="servingcell"', b'AT+CGSN' ]:
        self.request_process(cmd, no_wait=True)
      self.prev_ts = int(ts) + 2 # 更新週期
      
#  def status_update(self,ts):
#    self.request_process(b'ATE0')
#    self.request_process(b'ATV1')
#    self.request_process(b'AT+CMEE=2')
#    self.request_process(b'AT+CGREG=2')
#    self.request_process(b'AT+QURCCFG="urcport","uart2"')
#    prev_ts = 0
#    while True:
#      this_ts = int(time.time())
#      if this_ts > prev_ts:
#        self.request_process(b'AT+CPIN?')
#        self.request_process(b'AT+CSQ')
#        self.request_process(b'AT+COPS?')
#        if self.request_process(b'AT+QSPN')['retcode'] == False:
#          self.status['QSPN'] = None
#        if self.request_process(b'AT+QCCID')['retcode'] == False:
#          self.status['QCCID'] = None
#        self.request_process(b'AT+QNWINFO')
#        if self.request_process(b'AT+CGPADDR=1')['retcode'] == False:
#          self.status['CGPADDR'] = None
#        self.request_process(b'AT+QENG="servingcell"')
#        self.status['CGSN'] = self.request_process(b'AT+CGSN')['recv']['data'][0]
#        self.organize()
#        #pprint(self.status,width=160)
#        prev_ts = this_ts + ts
#      #else:
#      #  print(".")
#      time.sleep(0.1)
#      
#  def start_2(self,ts):
#    if self.thread_2 is None or self.thread.isAlive() is False:
#      self.thread_2 = threading.Thread(target=self.status_update, args=(ts,))
#      self.thread_2.daemon = True
#      self.thread_2.start()
#    return self.thread_2
    
if __name__ == '__main__':
  import time, resource
  from pprint import pprint
  target = Service({'port': '/dev/smd9'})
  target.start()
  target.logging = target._null
  prev_ts = 0
  while True:
    this_ts = int(time.time())
    if this_ts > prev_ts:
      target.organize()
      pprint(target.status,width=160)
      prev_ts = this_ts + 3
      #usage_memory = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
      #print('程式記憶體使用量: %s MB' % format( float(usage_memory) / 1024, '0,.2f'))
    time.sleep(1)
    