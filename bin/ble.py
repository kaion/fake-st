#!/usr/bin/python
# -*- coding: utf-8 -*-
from BaseUartService import UartService
import logging
import urllib.request
import traceback

class Service(UartService):
  fw_buffer = None
  def __init__(self, dicargs):
    UartService.__init__(self, dicargs)
    #print(dicargs.get('reader', None))
    self.reader = dicargs.get('reader', None)
    self.lte = dicargs.get('lte', None)
    self.version = dicargs.get('version', 'UNKNOWN')
    self.fake_st = True
    
  def do_20(self, this_requ):
    # 1-1 播放Audio CMD (0x20) 
    self.logging(self.logname, logging.INFO, 'BLE 要求播放音效, 音軌: %d, 音量: %d .' % (this_requ['data'][0],this_requ['data'][1]) )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, this_requ['data'] ))
    
  def do_21(self, this_requ):
    # 1-11  Read ST Version(0x21)
    self.logging(self.logname, logging.INFO, 'BLE 索取 ST Ver.' )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, str.encode(self.version) + b'\x01'))
    
  def do_22(self, this_requ):
    # 1-2 Audio Index CMD (0x22) 
    self.logging(self.logname, logging.INFO, 'BLE 索取 Audio Ver .' )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x01' + b'\x00\x00\x00\x01' + b'\x01'))
    
  def do_23(self, this_requ):
    # 1-16  Audio Stop  CMD (0x23) 
    self.logging(self.logname, logging.INFO, 'BLE 要求停止音效. (未實作)' )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x01' )) #不確定
    
  def do_24(self, this_requ):
    # 1-21 Delete Voice CMD (0x24)
    self.logging(self.logname, logging.INFO, 'BLE 要求清空音效. (不實作)' )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x03' )) 
    
  def do_41(self, this_requ):
    # 1-13  Reader TxRx CMD(0x41)
    self.logging(self.logname, logging.INFO, 'BLE 發送票證透通: %s' % self.hex_conv_string(this_requ['data']) )
    if self.reader != None:
      data = self.reader.request_process(this_requ['data'])
      #print(data)
      if data.get('recv') != None and data['recv'].get('data') != None:
        return self.sign_res( None, True, data=self.ble_response_combination(this_requ, data['recv'].get('data')))
    else:
      self.logging(self.logname, logging.INFO, '票證透通函式未初始化.' )
    #return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x00\x00\x02\x90\x00\x92'))
    return self.sign_res( None, False )
    
  def do_50(self, this_requ):
    # 1-3 4G restart CMD (0x50)
    self.logging(self.logname, logging.INFO, 'BLE 要求 4G 重啟.' )
    return self.sign_res( None, True, data=[self.ble_response_combination(this_requ, b'\x01'), self.ble_response_combination(this_requ, b'\x0b', forceCommand = b'\x51' )])
    
  def do_51(self, this_requ):
    # 1-4 4G Tx CMD (0x51)
    data = this_requ['data']
    this = {}
    this["url_length"] = int.from_bytes(data[0:2], byteorder='big', signed=False)
    ul = this["url_length"]
    this["url"] = data[2:ul+2].decode()
    this["len_Content-Length"] = int.from_bytes(data[ul+2:ul+3], byteorder='big', signed=False)
    this["Content-Length"] = data[ul+3:ul+3+this["len_Content-Length"]].decode()
    this["body"] = data[-int(this['Content-Length']):].decode()
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x17' + self.url_request(this["url"],this["body"]) ))
    
  def do_52(self, this_requ):
    # 1-9  Read 4G IMEI CMD(0x52)
    self.logging(self.logname, logging.INFO, 'BLE 索取 IEMI .' )
    if self.lte != None:
      IEMI = self.lte.status.get('CGSN')
      if IEMI != None:
        return self.sign_res( None, True, data=self.ble_response_combination( this_requ, str.encode( IEMI ) + b'\x01' ))
    elif self.fake_st == True:
      return self.sign_res( None, True, data=self.ble_response_combination(this_requ, str.encode('012345678901234') + b'\x01'))
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, str.encode('000000000000000') + b'\x00'))
    
  def do_53(self, this_requ):
    # 1-10  Read 4G ICCID CMD(0x53)
    self.logging(self.logname, logging.INFO, 'BLE 索取 ICCID .' )
    if self.lte != None:
      ICCID = self.lte.status.get('QCCID')
      if ICCID != None:
        return self.sign_res( None, True, data=self.ble_response_combination( this_requ, str.encode( ICCID ) + b'\x01' ))
    elif self.fake_st == True:
      return self.sign_res( None, True, data=self.ble_response_combination(this_requ, str.encode('01234567890123456789') + b'\x01')) 
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, str.encode('00000000000000000000') + b'\x01')) #b'\x00' 會被忽略
    
  def do_54(self, this_requ):
    # 1-12  Read RSSI CMD(0x54)
    self.logging(self.logname, logging.INFO, 'BLE 索取訊號強度.' )
    if self.lte != None:
      self.lte.organize()
      qeng = self.lte.status['organize'].get('QENG')
      if qeng != None:
        RSRP = qeng.get("rsrp")
        RSRQ = qeng.get("rsrq")
        RSSI = qeng.get("rssi")
        SINR = qeng.get("sinr")
        if RSRP != None and RSRQ != None and RSSI != None and SINR != None:
          return self.sign_res( None, True, data=self.ble_response_combination(this_requ, str.encode( ','.join((RSRP,RSRQ,RSSI,SINR)) ) + b'\x01'))
    elif self.fake_st == True:
      return self.sign_res( None, True, data=self.ble_response_combination(this_requ, str.encode('-93,-23,-15,20') + b'\x01')) 
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'---,---,000,---\x00' ))
    
  def do_55(self, this_requ):
    # 1-17  Dowload FW  CMD (0x55) 
    self.logging(self.logname, logging.INFO, 'BLE 請求韌體更新.' )
    data = this_requ['data']
    # 有分兩種, ST 要更新的和 BLE 要更的.
    this = {}
    this["target"] = int.from_bytes(data[0:1], byteorder='big', signed=False)
    this["url_length"] = int.from_bytes(data[1:3], byteorder='big', signed=False)
    ul = this["url_length"]
    this["url"] = data[3:ul+3].decode()
    if True:
      this["len_Content-Length"] = int.from_bytes(data[ul+3:ul+4], byteorder='big', signed=False)
      this["Content-Length"] = data[ul+4:ul+4+this["len_Content-Length"]].decode()
      this["body"] = data[-int(this['Content-Length']):].decode()
    else:
      # 問傑米哈他怎麼了...
      if len(data[ul+3:]) >= 100+4:
        this["Content-Length"] = data[ul+4:ul+4+3].decode()
        this["body"] = data[ul+4+3:].decode()
      elif len(data[ul+3:]) >= 10+3:
        this["Content-Length"] = data[ul+4:ul+4+2].decode()
        this["body"] = data[ul+4+2:].decode()
      else:
        this["Content-Length"] = data[ul+4:ul+4+1].decode()
        this["body"] = data[ul+4+1:].decode()
        
    self.fw_buffer = self.url_request(this["url"],this["body"])[1:]    #第一個 byte 要捨棄...= =
    #print(this["target"])
    if this["target"] == 0x05:
      if self.reader != None:
        return self.sign_res( None, True, data=self.ble_response_combination(this_requ, self.upgrade_reader_fw(this_requ, self.fw_buffer)))
      else:
        self.logging(self.logname, logging.INFO, '票證透通函式未初始化.' )
        return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\xE3'))
        
    if this["target"] == 0x03 or this["target"] == 0x01 :
      self.logging(self.logname, logging.INFO, '不支援 0x%02x 設備的更新.' % this["target"])
      return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\xE2'))
      
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, str(len(self.fw_buffer)).encode()))
    
  def do_56(self, this_requ):
    # 1-18  Transmit FW  CMD (0x56)
    start_idx, data_size = this_requ['data'].decode().split(',',1)
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, self.fw_buffer[int(start_idx):int(start_idx)+int(data_size)]))
    
  def do_57(self, this_requ):
    # 1-19 4G TxRx CMD (0x57)
    data = this_requ['data']
    this = {}
    this["url_length"] = int.from_bytes(data[0:2], byteorder='big', signed=False)
    ul = this["url_length"]
    this["url"] = data[2:ul+2].decode()
    this["len_Content-Length"] = int.from_bytes(data[ul+2:ul+3], byteorder='big', signed=False)
    this["Content-Length"] = data[ul+3:ul+3+this["len_Content-Length"]].decode()
    this["body"] = data[-int(this['Content-Length']):].decode()
    for name in ['url_length','url',"len_Content-Length","Content-Length","body"]:
      self.logging(self.logname, logging.DEBUG, '%-20s: %s' % ( name, this[name]) )
      
    if int(this["Content-Length"]) != len( this["body"]):
      self.logging(self.logname, logging.WARNING, 'this["Content-Length"] != len(this["body"]). (%d != %d)' % (int(this["Content-Length"]),len( this["body"])))
      
    elif this["len_Content-Length"] != len(this["Content-Length"]):
      self.logging(self.logname, logging.WARNING, 'this["len_Content-Length"] != len(this["Content-Length"]). (%d != %d)' % (this["len_Content-Length"], len(this["Content-Length"])) )
      
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, self.url_request(this["url"],this["body"],logging_response=True)))
    
  def do_58(self, this_requ):
    # 1-6  4G Sleep CMD (0x58)
    self.logging(self.logname, logging.INFO, 'BLE 要求 4G %s. (未實踐)' % ( 'WAKE' if this_requ['data'] == b'\x00' else 'SLEEP' ) )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x01'))
    
  def do_5A(self, this_requ):
    # 1-23 Get CPIN CMD (0x5A)
    self.logging(self.logname, logging.INFO, 'BLE 索取 4G SIM 狀況.' )
    if self.lte != None:
      CME = self.lte.status.get('CME','100')
      return self.sign_res( None, True, data=self.ble_response_combination( this_requ, str.encode( CME ) ))
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'404'))
    
  def do_60(self, this_requ):
    # 1-5 Read GPS CMD (0x60)
    self.logging(self.logname, logging.INFO, 'BLE 索取 GPS 座標. (未實踐)' )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x00'))
    
 
  def do_61(self, this_requ):
    # 1-14  GPS Sleep CMD (0x61)
    self.logging(self.logname, logging.INFO, 'BLE 要求 GPS %s. (未實踐)' % ( 'WAKE' if this_requ['data'] == b'\x01' else 'SLEEP' ) )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x01'))
    
  def do_62(self, this_requ):
    #1-15  Read GPS Version CMD (0x62)
    self.logging(self.logname, logging.INFO, 'BLE 索取 GPS Ver.' )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'FAKE-ST-GPS' + b'\x00'))
    
  def do_63(self, this_requ):
    # 1-22 GPS Save CMD (0x63)
    self.logging(self.logname, logging.INFO, 'BLE 要求%s紀錄 GPS 軌跡.' % ( '開始' if this_requ['data'][0:1] == b'\x01' else '結束' ) )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x01'))
    
  def do_70(self, this_requ):
    # GEV system on/off CMD(0x70)
    self.logging(self.logname, logging.INFO, 'BLE 要求 G-SYSTEM %s.' % ( 'OFF' if this_requ['data'] == b'\x00' else 'ON' ) )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b'\x01'))
    
  def do_71(self, this_requ):
    # 1-8  Read GEV CAN CMD(0x71)
    self.logging(self.logname, logging.INFO, 'BLE 要求 CAN 資料. (未實踐)' )
    TRIP_TIME = b'\x00\x00'              # 自行車單次騎乘時間   0~65535 minutes  LSB->MSB 
    ODOGRAPH = b'\x00\x00'               # 自行車總騎乘里程數  0~65535 km  LSB->MSB 
    BATTERY_CAPACITY = b'\x64'           # 電池容量  0~100%
    REMAIN_CAPACITY = b'\x00\x00'        # 電池剩餘的實時電量 0~65535 mAh LSB->MSB 
    BATTERY_CHARGING_CYCLE = b'\x00\x00' # 電池充電循環 0~65535 cycles LSB->MSB 
    BATTERY_LIFE = b'\x00'               # 電池壽命 0~100%
    REMAINING_RANGE = b'\x00'            # 剩餘里程 0~255km
    BIKE_SPEED = b'\x00\x00'             # 騎乘速度0~65535 km/hr  LSB->MSB 
    BATTERY_ID = b'HELLO___YOUBIKE_'     # ASCII
    data = ( TRIP_TIME,ODOGRAPH,BATTERY_CAPACITY,REMAIN_CAPACITY,BATTERY_CHARGING_CYCLE,BATTERY_LIFE,REMAINING_RANGE,BIKE_SPEED,BATTERY_ID )
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, b''.join(data)))
    
  def do_72(self, this_requ):
    # 1-24  Read BATTARY CAN CMD(0x72)
    self.logging(self.logname, logging.INFO, 'BLE 要求 BATTERY 資料. (未實踐)' )
    if True:
      BATTERY_CAPACITY = b'\x64'           # 電池容量  0~100%
      REMAIN_CAPACITY = b'\x00\x00'        # 電池剩餘的實時電量 0~65535 mAh LSB->MSB 
      BATTERY_CHARGING_CYCLE = b'\x00\x00' # 電池充電循環 0~65535 cycles LSB->MSB 
      BATTERY_LIFE = b'\x00'               # 電池壽命 0~100%
      REMAINING_RANGE = b'\x00'            # 剩餘里程 0~255km
      BIKE_SPEED = b'\x00\x00'             # 騎乘速度0~65535 km/hr  LSB->MSB 
      BATTERY_ID = b'HELLO___YOUBIKE_'     # ASCII
      data = b''.join(( BATTERY_CAPACITY,REMAIN_CAPACITY,BATTERY_CHARGING_CYCLE,BATTERY_LIFE,REMAINING_RANGE,BIKE_SPEED,BATTERY_ID ))
    else:
      data = ( b'\xEE' )
      
    return self.sign_res( None, True, data=self.ble_response_combination(this_requ, data))
    
  def upgrade_reader_fw( self, this_requ, buffer ):
    try:
      __reader_test__ = False
      _SUCCESS_ = b'\x00\x00\x02\x90\x00\x92'
      this_HASH = buffer[0:64]
      this_FW = buffer[64:]
      FW_Dowload_PAGE_Siz = len(this_FW) # 前64byte是 hash 簽章
      g_Dowload_total_Count = int(FW_Dowload_PAGE_Siz/512)
      if FW_Dowload_PAGE_Siz % 512 > 0: 
        g_Dowload_total_Count += 1
        while True:
          this_FW += b'\xff'
          if len(this_FW) % 512:
            break
      if not __reader_test__ :
        self.console_write_fixed( self.ble_response_combination(this_requ, str(len(self.fw_buffer)).encode()) )
      
      g_Updata_total_Count = int(g_Dowload_total_Count)
      g_UART3_send_buffer = b'\x00\x00\x04\x83\x70' + bytes([g_Updata_total_Count & 0xff]) + bytes([g_Updata_total_Count >> 8 ])
      g_UART3_send_buffer += bytes([self.chksum_calc(g_UART3_send_buffer)])
      data = self.reader.request_process(g_UART3_send_buffer)
      if data.get('recv') != None and data['recv'].get('data') != _SUCCESS_:
        raise 'F1'
      elif not __reader_test__ :
        self.console_write_fixed( self.ble_response_combination( this_requ, b'\x01') )

      for g_Updata_total_new_Count in range(0,g_Dowload_total_Count):
        g_UART3_send_buffer = b'\x00\x00\xff\x83\x71' + bytes([g_Updata_total_new_Count & 0xff]) + bytes([g_Updata_total_new_Count >> 8 ]) + b'\x00\x02'
        g_UART3_send_buffer +=  this_FW[512*g_Updata_total_new_Count:512*(g_Updata_total_new_Count+1)]
        g_UART3_send_buffer += bytes([self.chksum_calc(g_UART3_send_buffer)])
        data = self.reader.request_process(g_UART3_send_buffer)
        if data.get('recv') != None and data['recv'].get('data') != _SUCCESS_:
          raise 'F2'
        elif not __reader_test__ :
          self.console_write_fixed( self.ble_response_combination( this_requ, g_Dowload_total_Count.to_bytes(3, byteorder="big",signed=False) + (g_Updata_total_new_Count+1).to_bytes(3, byteorder="big",signed=False) + b'\x02') )
          
      g_UART3_send_buffer = b'\x00\x00\x04\x83\x73\x0F\x00'
      g_UART3_send_buffer += bytes([self.chksum_calc(g_UART3_send_buffer)])
      if data.get('recv') != None and data['recv'].get('data') != _SUCCESS_:
        raise 'F3'
      
      g_UART3_send_buffer = b'\x00\x00\x45\x83\x72\x0F\x00\x40' + this_HASH
      g_UART3_send_buffer += bytes([self.chksum_calc(g_UART3_send_buffer)])
      data = self.reader.request_process(g_UART3_send_buffer)
      data = self.reader.request_process(g_UART3_send_buffer)
      if data.get('recv') != None and data['recv'].get('data') != _SUCCESS_:
        raise 'F4'
      elif not __reader_test__ :
        return b'\x03'
          
    except:
      self.logging(self.logname, logging.INFO, '其他異常發生:\n%s' % ( traceback.format_exc() ) )
      return b'\xE1'
        
  def ble_response_combination( self, this_requ, data, **dicargs ):
    this_resp = {}
    this_resp['STX'] = bytes([this_requ['STX'][0] + 1 ])
    this_resp['Len'] = (len(data)+1).to_bytes(len(this_requ['Len']), byteorder="big")
    this_resp['Command'] = dicargs.get('forceCommand', this_requ['Command'])
    if type(data) != bytes:
      this_resp['data'] = str.encode(data)
    else: 
      this_resp['data'] = data
    this_resp['CRC'] =  bytes([self.chksum_calc( this_resp['STX'] + this_resp['Len'] + this_resp['Command'] + this_resp['data'] )])
    this_resp['ETX'] = b'\x2b'
    
    return  this_resp['STX'] + this_resp['Len'] + this_resp['Command'] + this_resp['data'] + this_resp['CRC'] + this_resp['ETX']
    
  def url_request(self, url, request_body = '', **dicargs):
    self.logging(self.logname, logging.INFO, 'HTTP Request:\n%s\n%s' % (url,request_body) )
    opener = urllib.request.build_opener()
    host = url.split('/')[2]
    request_headers = { 
      'content-type' : 'application/x-www-form-urlencoded'
    }
    if len(request_body) >= 1 :
      www = urllib.request.Request(url, str.encode(request_body), request_headers)
    else:
      www = urllib.request.Request(url, None, request_headers)
    try:
      response = opener.open(www,timeout=60)
      response_body = response.read()
      if dicargs.get('logging_response') == True:
        self.logging(self.logname, logging.INFO, 'HTTP Response:\n%s' % (response_body.decode()) )
      return b'\x17' + response_body
      
    except urllib.error.HTTPError as e:
      self.logging(self.logname, logging.INFO, '主機回應異常: %3d %s.' % ( e.code, e.reason ) )
      return str.encode( '%3d' % e.code )
      
    except urllib.error.URLError as e:
      self.logging(self.logname, logging.INFO, '網路連線異常: %s.' % ( e.reason ) )
      return b'709'
      
    except:
      self.logging(self.logname, logging.INFO, '其他異常發生:\n%s' % ( traceback.format_exc() ) )
      return b'999'
    

  def request_protocol_format( self, data ):
    plen = self.is_len_byte(data)
    #print(plen)
    syntax = {}
    if plen > 0:
      syntax['STX'] = data[0:1]
      syntax['Len'] = data[1:1+plen]
      syntax['Command'] = data[1+plen:2+plen]
      syntax['data'] = data[2+plen:-2]
      syntax['CRC'] = data[-2:-1]
      syntax['_CRC'] =  bytes([self.chksum_calc(data[:-2])])
      if syntax['CRC'] != syntax['_CRC']:
        self.logging(self.logname, logging.WARNING, 'syntax["CRC"] != syntax["_CRC"], 0x%02x != 0x%02x .' % (syntax["CRC"],syntax["_CRC"]) )
      syntax['ETX'] = data[-1:]
      return self.sign_res( None, True, data=data, syntax=syntax )
      
    self.logging(self.logname, logging.INFO, '無法解析 %s 格式' % self.hex_conv_string( data ) )
    return self.sign_res( None, False, data=data )
    
  def cmd_process(self):
    this_requ = self.get_tree()
    this_tid = self.get_tid()
    self.history[this_tid] = this_requ
    self.logging(self.logname, logging.INFO, '收到 %s . @%s' % ( self.hex_conv_string(self.rx_buffer), this_tid.split('-')[1] ) )
    self.logging(self.logname, logging.DEBUG, '申請 %s 處理.' % ( this_tid ) )
    this_requ['recv'] = self.request_protocol_format(self.rx_buffer)
      
    this_command = this_requ['recv']['syntax'].get('Command')
    if this_command != None and len( this_command ) == 1:
      mname = 'do_%02X' % this_command[0]
      if not hasattr(self, mname):
        self.logging(self.logname, logging.INFO,'沒實作 %02x 指令' % this_command[0] )
        this_requ['send'] = self.sign_res( None, False )
      
      else:
        method = getattr(self, mname)
        #this_requ['send'] = self.sign_res( None, True, data=method(this_requ['recv']['syntax']))
        this_requ['send'] = method(this_requ['recv']['syntax'])
     
    if this_requ['recv']['_res'] == True and this_requ['send']['_res'] == True:
      self.sign_res( this_requ, self.tx_process(this_tid) )
      
    else:
      self.logging(self.logname, logging.DEBUG, '指令處理異常, 放棄 %s 交易.' % ( this_tid ))
      self.sign_res( this_requ, False )
    
    self.rx_buffer = b''
    
  def rx_process(self):
    data = self.rx_buffer
    if data[-1:] == b'\x2b' and len(data) >= 5 and bytes([self.chksum_calc(data[:-2])]) == data[-2:-1]:
      #頭尾符合
      self.cmd_process()
    elif data[-1:] == b'\x2b' and len(data) >= 5:
      for p in range(0,len(data)):
        if ( self.is_stx_byte(data[p]) ) and ( self.is_len_byte(data[p:]) ):
          self.logging(self.logname, logging.WARNING, '拋棄 ' + self.hex_conv_string(data[:p]) )
          self.rx_buffer = data[p:]
          self.cmd_process()
          break
          
    elif ( len( data ) == 1 and not self.is_stx_byte(data[0]) ): # or ( data != b'' and self.rx_byte == b'' ):
      self.logging(self.logname, logging.DEBUG, '拋棄 ' + self.hex_conv_string(data) )
      self.rx_buffer = b''
      
  def is_stx_byte( self, byte ):
    correct_byte = [ 0x15, 0x25, 0x35 ]
    if byte in correct_byte :
      #print('0x%02x' % byte)
      return True
    return False
    
  def is_len_byte( self, data ):
    lentotal = len(data) - 3 #STX, CRC, ETX
    len2byte = int.from_bytes(data[1:3], byteorder='big', signed=False)
    len1byte = int.from_bytes(data[1:2], byteorder='big', signed=False)
    if len2byte == ( lentotal - 2 ) or len2byte == ( lentotal - 1 ): #@$@#!$%!%$#^#.....
      return 2
      
    elif len1byte == (lentotal - 1):
      return 1
      
    return 0
    
  def console_write( self, data, this_tid ):
    self.console.write(data)
    self.logging(self.logname, logging.INFO, '送出 %s . @%s' % ( self.hex_conv_string(data), this_tid.split('-')[1] ) )
    
  def console_write_fixed( self, data ):
    UartService.console_write(self, data)
    
if __name__ == '__main__':
  import time
  from pprint import pprint
  import reader
  #ble = Service({'port':'/dev/ttyUSB0'})
  b2 = reader.Service({'port':'/dev/ttyUSB0'})
  b2.start()
  #b1 = ()
  url = b'https://rd-test-mg.youbike2.com/api/ubikeV3/downloadfile'
  body = b'sid=0211e144d9650b3a24b933e11f8cdec384f8d806&data={"filename":"Hash-MP-1512_YouBike_20_v1_0_b3.bin"}'
  b1 = Service({})
  b1.reader = b2
  b1.rx_buffer = b'\x35\x30\x35\x5c\x22\x2c\x5c\x22\x62\x69\x6b\x65\x5f\x67\x70\x73\x5c\x22\x3a\x5c\x22\x30\x2e\x30\x2c\x30\x2e\x30\x5c\x22\x2c\x5c\x22\x62\x69\x6b\x65\x5f\x6d\x61\x63\x68\x69\x6e\x65\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x41\x75\x67\x20\x20\x37\x20\x32\x30\x31\x39\x31\x38\x3a\x35\x39\x3a\x31\x36\x5c\x22\x2c\x5c\x22\x49\x4d\x45\x49\x5c\x22\x3a\x5c\x22\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x30\x31\x32\x33\x34\x5c\x22\x2c\x5c\x22\x69\x73\x73\x75\x65\x5f\x74\x79\x70\x65\x5c\x22\x3a\x5c\x22\x36\x31\x30\x30\x30\x30\x30\x30\x5c\x22\x2c\x5c\x22\x72\x65\x61\x64\x65\x72\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x4d\x50\x2d\x31\x35\x31\x32\x5f\x59\x6f\x75\x42\x69\x6b\x65\x5f\x32\x30\x5f\x76\x31\x5f\x30\x5f\x62\x36\x5c\x22\x2c\x5c\x22\x42\x4c\x45\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x4d\x50\x2d\x31\x37\x31\x31\x5f\x42\x4c\x45\x5f\x59\x42\x5f\x76\x31\x5f\x31\x5f\x62\x33\x5c\x22\x2c\x5c\x22\x53\x54\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x76\x31\x2e\x30\x2e\x30\x2d\x33\x38\x2d\x67\x34\x63\x36\x36\x63\x65\x30\x5c\x22\x2c\x5c\x22\x67\x72\x6f\x75\x70\x5f\x6e\x6f\x5c\x22\x3a\x5c\x22\x31\x5c\x22\x2c\x5c\x22\x4c\x4f\x43\x4b\x42\x4f\x58\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x76\x31\x5f\x30\x5f\x62\x33\x31\x5c\x22\x2c\x5c\x22\x41\x55\x44\x49\x4f\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x31\x5f\x31\x5c\x22\x2c\x5c\x22\x72\x65\x74\x75\x72\x6e\x5f\x74\x69\x6d\x65\x5c\x22\x3a\x5c\x22\x31\x35\x36\x35\x36\x37\x36\x32\x34\x38\x5c\x22\x2c\x5c\x22\x73\x5f\x69\x64\x5c\x22\x3a\x5c\x22\x33\x32\x36\x33\x39\x37\x31\x37\x42\x33\x5c\x22\x2c\x5c\x22\x73\x74\x61\x74\x75\x73\x5f\x64\x69\x73\x61\x62\x6c\x65\x5c\x22\x3a\x5c\x22\x30\x5c\x22\x7d\x22\x7d\x8a\x2b\x15\x03\xb5\x57\x00\x45\x68\x74\x74\x70\x73\x3a\x2f\x2f\x72\x64\x2d\x74\x65\x73\x74\x2d\x6d\x67\x2e\x79\x6f\x75\x62\x69\x6b\x65\x32\x2e\x63\x6f\x6d\x2f\x61\x70\x69\x2f\x75\x62\x69\x6b\x65\x56\x33\x2f\x64\x61\x69\x6c\x79\x72\x65\x70\x6f\x72\x74\x3f\x68\x77\x69\x64\x3d\x42\x31\x38\x30\x30\x35\x30\x35\x03\x38\x37\x33\x73\x69\x64\x3d\x30\x32\x31\x31\x65\x31\x34\x34\x64\x39\x36\x35\x30\x62\x33\x61\x32\x34\x62\x39\x33\x33\x65\x31\x31\x66\x38\x63\x64\x65\x63\x33\x38\x34\x66\x38\x64\x38\x30\x36\x26\x64\x61\x74\x61\x3d\x7b\x22\x71\x75\x65\x72\x79\x22\x3a\x22\x7b\x5c\x22\x62\x69\x6b\x65\x5f\x6e\x6f\x5c\x22\x3a\x5c\x22\x33\x32\x36\x33\x30\x30\x46\x31\x46\x33\x5c\x22\x7d\x22\x2c\x22\x75\x70\x64\x61\x74\x65\x22\x3a\x22\x7b\x5c\x22\x46\x4f\x4e\x54\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x5c\x22\x2c\x5c\x22\x62\x61\x74\x74\x65\x72\x79\x5f\x49\x44\x5c\x22\x3a\x5c\x22\x5c\x22\x2c\x5c\x22\x61\x70\x69\x5c\x22\x3a\x5c\x22\x75\x70\x6c\x6f\x61\x64\x5c\x22\x2c\x5c\x22\x61\x70\x69\x5f\x64\x61\x74\x61\x5f\x74\x79\x70\x65\x5c\x22\x3a\x5c\x22\x38\x5c\x22\x2c\x5c\x22\x64\x65\x76\x69\x63\x65\x5f\x73\x74\x61\x74\x75\x73\x5c\x22\x3a\x5c\x22\x62\x6f\x6f\x74\x5c\x22\x2c\x5c\x22\x73\x6f\x6c\x61\x72\x5f\x73\x74\x61\x74\x75\x73\x5c\x22\x3a\x5c\x22\x32\x5c\x22\x2c\x5c\x22\x62\x61\x74\x74\x65\x72\x79\x5f\x63\x61\x70\x61\x63\x69\x74\x79\x5c\x22\x3a\x5c\x22\x30\x5c\x22\x2c\x5c\x22\x61\x70\x69\x5f\x74\x79\x70\x65\x5c\x22\x3a\x5c\x22\x72\x65\x71\x75\x65\x73\x74\x5c\x22\x2c\x5c\x22\x62\x61\x74\x74\x65\x72\x79\x5c\x22\x3a\x5c\x22\x31\x30\x30\x5c\x22\x2c\x5c\x22\x49\x43\x43\x49\x44\x5c\x22\x3a\x5c\x22\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x5c\x22\x2c\x5c\x22\x62\x61\x74\x74\x65\x72\x79\x5f\x6c\x69\x66\x65\x5c\x22\x3a\x5c\x22\x30\x5c\x22\x2c\x5c\x22\x62\x61\x74\x74\x65\x72\x79\x5f\x63\x68\x61\x72\x67\x69\x6e\x67\x5f\x63\x79\x63\x6c\x65\x5c\x22\x3a\x5c\x22\x30\x5c\x22\x2c\x5c\x22\x62\x69\x6b\x65\x5f\x6e\x6f\x5c\x22\x3a\x5c\x22\x33\x32\x36\x33\x30\x30\x46\x31\x46\x33\x5c\x22\x2c\x5c\x22\x62\x61\x74\x74\x65\x72\x79\x5f\x6d\x69\x6c\x65\x61\x67\x65\x5c\x22\x3a\x5c\x22\x30\x5c\x22\x2c\x5c\x22\x64\x65\x76\x69\x63\x65\x5f\x69\x64\x5c\x22\x3a\x5c\x22\x62\x31\x38\x30\x30\x35\x30\x35\x5c\x22\x2c\x5c\x22\x62\x69\x6b\x65\x5f\x67\x70\x73\x5c\x22\x3a\x5c\x22\x30\x2e\x30\x2c\x30\x2e\x30\x5c\x22\x2c\x5c\x22\x62\x69\x6b\x65\x5f\x6d\x61\x63\x68\x69\x6e\x65\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x41\x75\x67\x20\x20\x37\x20\x32\x30\x31\x39\x31\x38\x3a\x35\x39\x3a\x31\x36\x5c\x22\x2c\x5c\x22\x49\x4d\x45\x49\x5c\x22\x3a\x5c\x22\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x30\x31\x32\x33\x34\x5c\x22\x2c\x5c\x22\x69\x73\x73\x75\x65\x5f\x74\x79\x70\x65\x5c\x22\x3a\x5c\x22\x36\x31\x30\x30\x30\x30\x30\x30\x5c\x22\x2c\x5c\x22\x72\x65\x61\x64\x65\x72\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x4d\x50\x2d\x31\x35\x31\x32\x5f\x59\x6f\x75\x42\x69\x6b\x65\x5f\x32\x30\x5f\x76\x31\x5f\x30\x5f\x62\x36\x5c\x22\x2c\x5c\x22\x42\x4c\x45\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x4d\x50\x2d\x31\x37\x31\x31\x5f\x42\x4c\x45\x5f\x59\x42\x5f\x76\x31\x5f\x31\x5f\x62\x33\x5c\x22\x2c\x5c\x22\x53\x54\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x76\x31\x2e\x30\x2e\x30\x2d\x33\x38\x2d\x67\x34\x63\x36\x36\x63\x65\x30\x5c\x22\x2c\x5c\x22\x67\x72\x6f\x75\x70\x5f\x6e\x6f\x5c\x22\x3a\x5c\x22\x31\x5c\x22\x2c\x5c\x22\x4c\x4f\x43\x4b\x42\x4f\x58\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x76\x31\x5f\x30\x5f\x62\x33\x31\x5c\x22\x2c\x5c\x22\x41\x55\x44\x49\x4f\x5f\x76\x65\x72\x5c\x22\x3a\x5c\x22\x31\x5f\x31\x5c\x22\x2c\x5c\x22\x72\x65\x74\x75\x72\x6e\x5f\x74\x69\x6d\x65\x5c\x22\x3a\x5c\x22\x31\x35\x36\x35\x36\x37\x36\x32\x38\x31\x5c\x22\x2c\x5c\x22\x73\x5f\x69\x64\x5c\x22\x3a\x5c\x22\x33\x32\x36\x33\x39\x37\x31\x37\x42\x33\x5c\x22\x2c\x5c\x22\x73\x74\x61\x74\x75\x73\x5f\x64\x69\x73\x61\x62\x6c\x65\x5c\x22\x3a\x5c\x22\x30\x5c\x22\x7d\x22\x7d\x8f\x2b'
  b1.rx_byte = b''
  b1.rx_process()
  
  while True:
    #pprint(b2.history,width=160)
    #print("-----")
    time.sleep(1)
    #reader.rx_byte = b"\x2b"
    #reader.rx_buffer = b"\x15\x00\xa7\x55\x04\x00\x38\x68\x74\x74\x70\x73\x3a\x2f\x2f\x72\x64\x2d\x74\x65\x73\x74\x2d\x6d\x67\x2e\x79\x6f\x75\x62\x69\x6b\x65\x32\x2e\x63\x6f\x6d\x2f\x61\x70\x69\x2f\x75\x62\x69\x6b\x65\x56\x33\x2f\x64\x6f\x77\x6e\x6c\x6f\x61\x64\x66\x69\x6c\x65\x02\x31\x30\x33\x73\x69\x64\x3d\x30\x32\x31\x31\x65\x31\x34\x34\x64\x39\x36\x35\x30\x62\x33\x61\x32\x34\x62\x39\x33\x33\x65\x31\x31\x66\x38\x63\x64\x65\x63\x33\x38\x34\x66\x38\x64\x38\x30\x36\x26\x64\x61\x74\x61\x3d\x7b\x22\x62\x69\x6b\x65\x5f\x6e\x6f\x22\x3a\x22\x33\x32\x36\x33\x30\x30\x46\x31\x46\x33\x22\x2c\x22\x66\x69\x6c\x65\x6e\x61\x6d\x65\x22\x3a\x22\x46\x6f\x6e\x74\x46\x69\x6c\x65\x5f\x76\x31\x2e\x62\x69\x6e\x22\x7d\xf9\x2b"
    #reader.rx_process()