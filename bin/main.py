#!/usr/bin/python3
# -*- coding: utf-8 -*-

if __name__ == '__main__':
  import logging, time, os, sys, resource #, dotenv
  from utils import logmanager, httpd
  import ble, reader, lte
  from threading import Thread
  from utils.tools import *
  from configparser import RawConfigParser
  from pprint import pprint
  check_repeated_execution(sys.argv[0])
  set_abspath_chdir(sys.argv[0])
  print('Starting server, use <Ctrl-C> to stop')
  start_version = get_version()
  
  config_ini = RawConfigParser ()
  if os.path.isfile('config.ini'): 
    config_ini.read('config.ini')
  else:
    config_ini.read('default_config.ini')
    
  plc ={
    'threads' : {
      'ble': {
        'kwargs': {
          'port': config_get( config_ini,'BLE','UART','/dev/ttyHS0'),
          'logging' : logmanager.save,
          'reader_handling' : None,
          'lte_status' : None
        }
      },
      'reader': {
        'kwargs': {
          'port': config_get( config_ini,'READER','UART','/dev/ttyHSL1'),
          'logging' : logmanager.save
        }
      },
      'lte': {
        'kwargs': {
          'port': config_get( config_ini,'LTE','UART','/dev/smd9'),
          'timeout': 0.125,
          'logging' : logmanager.save
        }
      },
      'logging': {
        'target': logmanager.service,
        'kwargs': {
          'console_output' : config_get(config_ini,'LOGGING','OUTPUT',None),
          'max_file_bytes' : config_get(config_ini,'LOGGING','MAX_FILE_BYTES',10) * 1024 * 1024,
          'max_directory_bytes' : config_get(config_ini,'LOGGING','MAX_DIRECTORY_BYTES',20) * 1024 * 1024,
        }
      }
    },
    'enable_sequence' : [
      'logging'
    ]
  }
  if config_get(config_ini,'READER','ENABLE',False) is True:
    plc['enable_sequence'].append('reader')
    
  if config_get(config_ini,'LTE','ENABLE',False) is True:
    plc['enable_sequence'].append('lte')
    
  if config_get(config_ini,'BLE','ENABLE',False) is True:
    plc['enable_sequence'].append('ble')
    
  plc['threads']['logging']['kwargs'].update({'task' : plc['enable_sequence']})
  thread_name_init('main')
  ts = 0
  # 最後要整理這段
  while True:
    for thread_name in plc['enable_sequence']:
      #print(thread_name)
      if plc['threads'].get(thread_name, None) != None:
        if thread_name == 'reader' and ( plc['threads'][thread_name].get('thread') is None or plc['threads'][thread_name]['thread'].isAlive() is False ):
          if plc['threads'][thread_name].get('class') == None:
            plc['threads'][thread_name]['class'] = reader.Service(plc['threads'][thread_name]['kwargs'])
          plc['threads'][thread_name]['thread'] = plc['threads'][thread_name]['class'].start(thread_name)
          plc['threads']['ble']['kwargs']['reader'] = plc['threads'][thread_name]['class']
          #plc['threads'][thread_name]['class'].logfunc = plc['threads'][thread_name]['class']._null
          
        elif thread_name == 'lte' and ( plc['threads'][thread_name].get('thread') is None or plc['threads'][thread_name]['thread'].isAlive() is False ):
          if plc['threads'][thread_name].get('class') == None:
            plc['threads'][thread_name]['class'] = lte.Service(plc['threads'][thread_name]['kwargs'])
          plc['threads'][thread_name]['thread'] = plc['threads'][thread_name]['class'].start(thread_name)
          plc['threads']['ble']['kwargs']['lte'] = plc['threads'][thread_name]['class']
          #plc['threads'][thread_name]['class'].logfunc = plc['threads'][thread_name]['class']._null
        
        elif thread_name == 'ble' and ( plc['threads'][thread_name].get('thread') is None or plc['threads'][thread_name]['thread'].isAlive() is False ):
          plc['threads'][thread_name]['kwargs']['version'] = start_version
          if plc['threads'][thread_name].get('class') == None:
            plc['threads'][thread_name]['class'] = ble.Service(plc['threads'][thread_name]['kwargs'])
          plc['threads'][thread_name]['thread'] = plc['threads'][thread_name]['class'].start(thread_name)

          #plc['threads'][thread_name]['class'].logfunc = plc['threads'][thread_name]['class']._print
          
        elif thread_name == 'logging' and ( plc['threads'][thread_name].get('thread') is None or plc['threads'][thread_name]['thread'].isAlive() is False ):
          plc['threads'][thread_name]['thread'] = Thread(target=plc['threads'][thread_name]['target'], kwargs=plc['threads'][thread_name]['kwargs'], name=thread_name)
          plc['threads'][thread_name]['thread'].daemon = True
          plc['threads'][thread_name]['thread'].start()
          time.sleep(1)
          
        elif plc['threads'][thread_name].get('thread', None) == None or plc['threads'][thread_name]['thread'].isAlive() is False:
          plc['threads'][thread_name]['thread'] = Thread(target=plc['threads'][thread_name]['target'], kwargs=plc['threads'][thread_name]['kwargs'], name=thread_name)
          plc['threads'][thread_name]['thread'].daemon = True
          plc['threads'][thread_name]['thread'].start()
          
    if time.time() > ts:
      if ts == 0:
        logging.getLogger('root').log( logging.INFO, '目前版本: %s .' % start_version )
        ts = time.time()
      ts = ts + ( 60 * 5 )
      usage_memory = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
      if usage_memory >= 20 * 1024:
        logging.getLogger('root').log( logging.WARNING, '程式記憶體使用量: %s MB .' % format( float(usage_memory) / 1024, '0,.2f') )
      else:
        logging.getLogger('root').log( logging.INFO, '程式記憶體使用量: %s MB .' % format( float(usage_memory) / 1024, '0,.2f') )
    now_version = get_version()
    #print(get_version())
    if now_version != start_version :
      logging.getLogger('root').log( logging.INFO, '出現新版本: %s , 準備重新啟動.' % now_version ) 
      exit(0) 
    #pprint(plc['threads']['ble']['class'].history, width=160)
    time.sleep(5)

