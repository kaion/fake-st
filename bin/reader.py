#!/usr/bin/python
# -*- coding: utf-8 -*-
from BaseUartService import UartService
import logging

class Service(UartService):
  rx_len = 0xff
    
  def rx_process(self):
    if len(self.rx_buffer) == self.rx_len:
      self.logging(self.logname, logging.INFO, '收到 ' + self.hex_conv_string(self.rx_buffer) )
      self.history[self.current_tid]['recv'] = self.response_paser( self.rx_buffer )
      self.sign_res( self.history[self.current_tid], self.history[self.current_tid]['recv']['_res'] )
      logging.getLogger('root.alpp').log( logging.DEBUG, '與對象的 %s 交易完成.' % ( self.current_tid ))
      # 非 loop 不能改 current_tid
      #self.current_tid = None
      self.rx_len = 0xff
      self.rx_buffer = b''
      
    elif len(self.rx_buffer) == 3:
      #資料長度: 第三個位元 + 前 3 BYTE + CRC
      self.rx_len = self.rx_byte[0] + 4 
      
    elif self.rx_buffer != b'' and self.rx_byte == b'':
      # timeout 拋棄原資料
      self.logging(self.logname, logging.INFO, '拋棄 ' + hex_conv_string(self.rx_buffer) )
      self.rx_buffer = b''
      
if __name__ == '__main__':
  import time
  from pprint import pprint
  reader = Service({'port': '/dev/ttyUSB0'})
  #reader.console_parameter(port='/dev/ttyUSB1') #{'port': '/dev/ttyUSB1'})
  reader.start()
  #time.sleep(1)
  reader.request_process(b'\x00\x00\x04\x91\x29\x00\x00\xbc',cache=True)
  #pprint(reader.history,width=160)
  reader.request_process(b'\x00\x00\x04\x91\x29\x00\x00\xbc',cache=True)
  #pprint(reader.history,width=160)
  reader.request_process(b'\x00\x00\x04\x91\x29\x00\x00\xbc')
  pprint(reader.history,width=160)
  while True:
    #pprint(reader.history,width=160)
    print("-----")
    time.sleep(10)